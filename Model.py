import csv

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import BernoulliNB
from sklearn import cross_validation
from sklearn.linear_model import LogisticRegression
import operator
from sklearn.metrics import classification_report


class DataSet(object):
    def __init__(self,**kwargs):
        opts = kwargs
        if opts.get('file_path'):
            with open(opts.get('file_path'),'rU') as csv_file:
                reader = csv.reader(csv_file,delimiter=",",quotechar='"')
                data =[]
                labels = []
                label_mapping = {}
                for row in reader:
                    # skip missing data
                    if len(row)==3:
                        data.append(row[0])
                        labels.append(row[1])
                        label_mapping[row[1]]=row[2]
                    else:
                        print "Error in data"
                        print row
                self.data = data
                self.labels = labels
                self.label_mapping = label_mapping
                if opts.get('split_data'):
                    test_size = opts.get('test_size',0.4)
                    random_state = opts.get('random_state',43)
                    self.train_x,self.test_x,self.train_y,self.test_y = cross_validation.train_test_split(self.data,self.labels,test_size=test_size,random_state=random_state)

    @classmethod
    def get_comment_tag_dataset_from_csv(cls,file_path,split_data=True,test_size=0.4,random_state=43):
        state_ = {'file_path': file_path, 'split_data': split_data, 'test_size': test_size, 'random_state': random_state}
        return cls(**state_)

    def get_label(self,idx):
        return self.label_mapping[idx]

class TagClassifierError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class TagClassifier(object):
    def __init__(self,dataset,binarize=False):
        self.dataset = dataset
        # initializing vectorizers to null
        if binarize:
            self.vectorizer = CountVectorizer(binary=binarize,ngram_range=(1,2),stop_words='english')
        else:
            self.vectorizer = TfidfVectorizer(binary=binarize,ngram_range=(1,2),smooth_idf=True,stop_words='english')
        self.initialize_classifier()

    # Method to be implemented by subclasses, should create a classifer instance self.classifier
    def set_classifier(self):
        raise NotImplementedError()

    def preprocess(self,data_tobe_preprocessed):
        return self.vectorizer.fit_transform(data_tobe_preprocessed)

    def apply_preprocessing_model(self,data_to_be_preprocessed):
        return self.vectorizer.transform(data_to_be_preprocessed)


    def predict(self,data_tobe_predicted):
        pp_data_tobe_predicted = self.apply_preprocessing_model(data_tobe_predicted)
        return self.classifier.predict(pp_data_tobe_predicted)

    def get_cv_avg_accuracy(self,fold=3):
        if self.dataset.train_x:
            cv_data = self.preprocess(self.dataset.train_x)
            cv_scores = cross_validation.cross_val_score(self.classifier,cv_data,self.dataset.train_y,cv=fold,scoring='accuracy')
            avg_accuracy = sum(cv_scores)*1.0/len(cv_scores)
            return avg_accuracy

    def learn_model(self):
        training = self.preprocess(data_tobe_preprocessed=self.dataset.train_x)
        self.classifier.fit(training,self.dataset.train_y)

class MNB(TagClassifier):
    def initialize_classifier(self):
        self.classifier = MultinomialNB()

class Logistic(TagClassifier):
    def initialize_classifier(self):
        self.classifier = LogisticRegression(C=1.0, penalty='l2')

class BNB(TagClassifier):
    def initialize_classifier(self):
        self.classifier = BernoulliNB(alpha=1.0)


def main():
    dataset = DataSet.get_comment_tag_dataset_from_csv('/home/venki/comment_tags.csv')
    models = {}
    models['mnb'] = MNB(dataset=dataset)
    models['bnb'] = BNB(dataset=dataset,binarize=True)
    models['logistic'] = Logistic(dataset=dataset)

    avg_accuracy_map = {}
    for model in models:
        avg_accuracy_map[model] = models[model].get_cv_avg_accuracy()

    best_model = sorted(avg_accuracy_map.iteritems(),key=operator.itemgetter(1),reverse=True)[0][0]
    print '*'*15
    print 'The best model is {0} with an average accuracy of {1}'.format(best_model,avg_accuracy_map[best_model])
    print '*'*15
    best_classifier = models[best_model]
    # learn model on
    best_classifier.learn_model()
    predicted = best_classifier.predict(dataset.test_x)
    print classification_report(predicted,dataset.test_y)

    with open('/home/venki/predicted_set.csv','w') as predicted_file:
        csv_writer = csv.writer(predicted_file,delimiter=',',quotechar='"')
        csv_writer.writerow(['message','actual','predicted'])
        for idx,message in enumerate(dataset.test_x):
             csv_writer.writerow([message,dataset.get_label(dataset.test_y[idx]),dataset.get_label(predicted[idx])])


main()